//  ViewController.swift
//  CoddeAutoLayout
//
//  Created by Olena Makhobei on 04.08.2021.
//

import UIKit

class ViewController: UIViewController {
    var textData : String!
    var view1 : UIView!
    var lblView : UILabel!
    var loadBtt : UIButton!
    var clearBtt : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        setContsraints()
        
    }
    func initViews(){
    self.view1 = UIView()
    self.lblView = UILabel()
    self.loadBtt = UIButton()
    self.clearBtt = UIButton()
        
        view1.translatesAutoresizingMaskIntoConstraints = false
        lblView.translatesAutoresizingMaskIntoConstraints = false
        loadBtt.translatesAutoresizingMaskIntoConstraints = false
        clearBtt.translatesAutoresizingMaskIntoConstraints = false
        
        //Set background color of elements
        view1.backgroundColor = UIColor.white
        lblView.backgroundColor = UIColor.white
        loadBtt.backgroundColor = UIColor.black
        clearBtt.backgroundColor = UIColor.black
        
        lblView.numberOfLines = 0
        lblView.text = textData
        lblView.textAlignment = .left
        lblView.sizeToFit()
        lblView.layer.masksToBounds = true
        lblView.layer.cornerRadius = 5
        
        loadBtt.setTitle("Load", for: .normal)
        loadBtt.layer.cornerRadius = 10
        loadBtt.addTarget(self, action: #selector(loadBttAction), for: .touchUpInside)
        
        clearBtt.setTitle("Clear", for: .normal)
        clearBtt.layer.cornerRadius = 10
        clearBtt.addTarget(self, action: #selector(clearBttAction), for: .touchUpInside)

        
        self.view.addSubview(view1)
        self.view.addSubview(lblView)
        self.view.addSubview(loadBtt)
        self.view.addSubview(clearBtt)
        
    }
    func setContsraints(){
        //Anchors for View
        view1.leadingAnchor.constraint(equalTo:view.leadingAnchor,constant: 0).isActive = true
        view1.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        view1.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        view1.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        
        //Anchors for the Label
        lblView.leadingAnchor.constraint(equalTo:view1.leadingAnchor,constant: 25).isActive = true
        lblView.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: -25).isActive = true
        lblView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -100).isActive = true
        lblView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        
        //Anchors for Load Button
        loadBtt.leadingAnchor.constraint(equalTo: view1.leadingAnchor, constant: 10).isActive = true
        loadBtt.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        
        //Anchors for Clear Button
        clearBtt.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -10).isActive = true
        clearBtt.trailingAnchor.constraint(equalTo: view1.trailingAnchor, constant: -10).isActive = true
       
        
        //Anchors for leading and trainling for buttons
        loadBtt.trailingAnchor.constraint(equalTo: clearBtt.leadingAnchor,constant: -50).isActive = true
        loadBtt.widthAnchor.constraint(equalTo: clearBtt.widthAnchor).isActive = true
        clearBtt.widthAnchor.constraint(equalTo: loadBtt.widthAnchor).isActive = true
        clearBtt.leadingAnchor.constraint(equalTo: loadBtt.trailingAnchor, constant: 50).isActive = true
        

    }

    @objc func clearBttAction(sender: UIButton!){
        lblView?.text = "This field is empty! Please,click button 'Load' for the loadind some data "
    }
    
    @ objc func loadBttAction(sender: UIButton!){
        lblView?.text = "The fundamental building block in Auto Layout is the constraint. Constraints express rules for the layout of elements in your interface; for example, you can create a constraint that specifies an elementТs width, or its horizontal distance from another element. You add and remove constraints, or change the properties of constraints, to affect the layout of your interface.When calculating the runtime positions of elements in a user interface, the Auto Layout system considers all constraints at the same time, and sets positions in such a way that best satisfies all of the constraints.Constraint Basics.You can think of a constraint as a mathematical representation of a human-expressable statement. If youТre defining the position of a button, for example, you might want to say Уthe left edge should be 20 points from the left edge of its containing view.Ф More formally, this translates to button.left = (container.left + 20), which in turn is an expression of the form y = m*x + b, where:"
    }
        // Do any additional setup after loading the view.
    }





